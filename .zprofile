# Export necessary environment variables and start
# the preferred window manager

# Exports
export EDITOR="/usr/bin/nvim"
export GTK_THEME="Rainforest" # Self made theme
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

export DVDCSS_CACHE="$XDG_DATA_HOME"/dvdcss
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GOPATH="$XDG_DATA_HOME"/go
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export HISTFILE="${XDG_STATE_HOME}"/bash/history
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export OCTAVE_SITE_INITFILE="$XDG_CONFIG_HOME/octave/octaverc"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export SQLITE_HISTORY="$XDG_CACHE_HOME"/sqlite_history
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo
export WINEPREFIX="$XDG_DATA_HOME"/wine
export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons

# This needs to be edited system specifically
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
	echo -n 'Which display protocol to use (X/W) '
	read protocol
	case $protocol in
		[xX])   export XDG_CURRENT_DESKTOP="dwm";
			exec startx ~/.xinitrc dwm 2&> /dev/null;;
		[wW])   export XDG_CURRENT_DESKTOP="dwl";
			exec /usr/bin/zsh -c 'dwl -s .local/bin/dwl-start.sh';;
		*)	echo "";
			echo "Welcome to Arch PC!";
			echo "Please exit (CTRL-D) to see this prompt again.";
			echo "";;
	esac
fi

