#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ARTIST_LEN 20
#define MAX_TITLE_LEN 25

enum STATUSES {
	OFFLINE = 0,
	STOPPED,
	PAUSED,
	PLAYING,
};

struct cmusState {
	char status;
	char artist[MAX_ARTIST_LEN];
	char title[MAX_TITLE_LEN];
} playerState = { 0 };

const char *stateToString(int status)
{
	const char *stringForm;

	switch (status)
	{
		case PLAYING:
			stringForm = "playing";
			break;
		case PAUSED:
			stringForm = "paused";
			break;
		case STOPPED:
			stringForm = "stopped";
			break;
		default:
			stringForm = "offline";
	}

	return stringForm;
}

int main(int argc, char *argv[])
{
	FILE *fp;
	char *line = NULL;
    size_t len = 0;
    ssize_t read;

	// cmus-remote works when cmus is running
	// TODO: user cmus-socket instead
	fp = popen("/usr/bin/cmus-remote --query 2>&1", "r");
	if (fp == NULL) {
		playerState.status = OFFLINE;
	}
	else
	{
		// Reads line by line from cmus-remote
		while ((read = getline(&line, &len, fp)) != -1)
		{
			// Remove the last line end character
			read--;

			if (strncmp("/usr/bin/cmus-remote: cmus is not running", line, 19) == 0)
			{
				playerState.status = OFFLINE;
			}

			// Different information that is passed to stdout
			if (strncmp("status", line, 6) == 0)
			{
				if (*(line + 7) == 's')
				{
					playerState.status = STOPPED;
				}
				else if (*(line + 8) == 'a')
				{
					playerState.status = PAUSED;
				}
				else if (*(line + 8) == 'l')
				{
					playerState.status = PLAYING;
				}
			}
			else if (strncmp("tag artist", line, 10) == 0)
			{
				read -= 11;

				// FIXME: Maybe forget the spacing entirely and rely on yambar to
				//        handle spacing
				if (read >= MAX_ARTIST_LEN)
				{
					// Add few dots
					playerState.artist[0] = '[';
					strncpy(playerState.artist + 1, line + 11, MAX_ARTIST_LEN - 3);
					strncpy(playerState.artist + MAX_ARTIST_LEN - 3, "..", 2);
				}
				else
				{
					memset(playerState.artist, ' ', MAX_ARTIST_LEN - read - 2);
					playerState.artist[MAX_ARTIST_LEN - read - 2] = '[';
					strncpy(playerState.artist + MAX_ARTIST_LEN - read - 1, line + 11, read);
				}
				playerState.artist[MAX_ARTIST_LEN - 1] = 0;
			}
			else if (strncmp("tag title", line, 9) == 0)
			{
				read -= 10;

				if (read + 1 >= MAX_TITLE_LEN)
				{
					// Add few dots
					strncpy(playerState.title, line + 10, MAX_TITLE_LEN - 4);
					strncpy(playerState.title + MAX_TITLE_LEN - 4, "..]", 3);
				}
				else
				{
					strncpy(playerState.title, line + 10, read);
					playerState.title[read] = ']';
					memset(playerState.title + read + 1, ' ', MAX_TITLE_LEN - read - 2);
				}
				playerState.title[MAX_TITLE_LEN - 1] = 0;
			}
		}

		if (line)
		{
			free(line);
		}
	}

	pclose(fp);

	if (playerState.status == OFFLINE)
	{
		printf("status|string|offline\n");
	}
	else 
	{
		printf("status|string|%s\n", stateToString(playerState.status));
		printf("artist|string|%s\n", playerState.artist);
		printf("title|string|%s\n", playerState.title);
	}

	printf("\n");
	fflush(stdout);

	return 0;
}
