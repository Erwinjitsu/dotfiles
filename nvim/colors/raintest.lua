-- Rainforest theme but in lua
-- Made to be compatible with Nvim v0.10.0

vim.cmd.highlight('clear')
vim.g.colors_name = 'rainforest'

-- From vim.lua
local hi = function(name --[[string]], val)
  -- Force links
  val.force = true

  -- Make sure that `cterm` attribute is not populated from `gui`
  val.cterm = val.cterm or {}

  -- Define global highlight
  vim.api.nvim_set_hl(0, name, val)
end

-- Normal text
hi('Normal', {fg = 0xd7ffff, ctermfg = 0xd7ffff})

-- Syntax highlight
hi('Comment', { fg = 0x7da861 , ctermfg = 0x7da861 })
hi('Constant', { fg = 0xe6ba00, ctermfg = 0xe6ba00, bold = true })
hi('Identifier', { fg = 0xe10531, ctermfg = 0xe10531, cterm = { bold = true } })
hi('Function', { fg = 0x00afd7, ctermfg = 0x00afd7, bold = true })
hi('Statement', { fg = 0xbecc00, ctermfg = 0xbecc00, bold = true })
hi('Type', { fg = 'magenta', ctermfg = 'magenta', bold = true })
hi('Special', { fg = 'lightred', ctermfg = 'lightred' })
hi('Delimeter', { fg = 'brown', ctermfg = 'brown' })
hi('Error', { fg = 0xffd700, bg = 'red', ctermfg = 0xffd700, ctermbg = 'red' })

-- Floats
--hi('NormalFloat',    { link = 'Comment' })

-- Preprocessor flags
hi('PreProc', { link = 'Constant' })
hi('Include', { link = 'Constant' })
hi('Define', { link = 'Constant' })
hi('Macro', { link = 'Constant' })
hi('PreCondit', { link = 'Constant' })

-- Types
hi('String',         { link = 'Comment' })
hi('Character',      { link = 'Comment' })
hi('Number',         { link = 'Comment' })
hi('Boolean',        { link = 'Comment' })

-- Keywords etc.
hi('Conditional',    { link = 'Statement' })
hi('Repeat',         { link = 'Statement' })
hi('Label',          { link = 'Statement' })
hi('Operator',       { link = 'Statement' })
hi('Keyword',        { link = 'Statement' })
hi('Exception',      { link = 'Statement' })

-- Fixes
hi('@lsp.type.variable', { link = 'Identifier' })

-- Git syntax highlight (nvimdiff)
hi('DiffAdd', { fg = 0xd7ffff, bg = 0x008700, ctermfg = 0xd7ffff, ctermbg = 0x008700, bold = true })
hi('DiffDelete', { fg = 0xd7ffff, bg = 0xaf0000, ctermfg = 0xd7ffff, ctermbg = 0xaf0000, bold = true })
hi('DiffChange', { fg = 0xd7ffff, bg = 0x008700, ctermfg = 0xd7ffff, ctermbg = 0x008700, bold = true })
hi('DiffText', { fg = 'green', bg = 0x005f00, ctermfg = 'green', ctermbg = 0x005f00, bold = true })

