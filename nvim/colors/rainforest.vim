" Originally based on colorscheme 'forest_refuge'
" from github user An4nke

" Reset highlighting
highlight clear
" Set default colors
if exists ("syntax_on")
	syntax reset
endif

" Set background variable
set background=dark
" Naming color scheme
let colors_name = "rainforest"

" Set new highlighting
" Normal text foreground, background
hi  Normal 	ctermfg=195 guifg=SeaGreen4 guibg=Black

" Syntax highlighting (:help group-name)
hi  Comment	cterm=NONE ctermfg=darkgreen gui=NONE guifg=honeydew
hi  Constant	cterm=NONE ctermfg=107 gui=NONE guifg=cornsilk4
hi  Identifier	cterm=NONE ctermfg=darkred gui=NONE guifg=LavenderBlush1
hi  Function	cterm=bold ctermfg=cyan gui=NONE guifg=DarkSalmon
hi  Statement	cterm=bold ctermfg=green gui=NONE guifg=chocolate4
hi  PreProc 	cterm=bold ctermfg=yellow gui=NONE guifg=DarkSeaGreen1
hi  Type	cterm=bold ctermfg=magenta gui=NONE guifg=OldLace
hi  Special	cterm=bold ctermfg=lightred gui=NONE guifg=brown
hi  Delimiter	cterm=NONE ctermfg=brown gui=NONE guifg=tomato
hi  Error	cterm=underline ctermbg=red ctermfg=yellow guifg=orange

" Git syntax highlight (nvimdiff)
hi  DiffAdd	cterm=bold ctermfg=195 ctermbg=28 gui=none guifg=bg
hi  DiffDelete	cterm=bold ctermfg=195 ctermbg=124 gui=none guifg=bg
hi  DiffChange	cterm=bold ctermfg=195 ctermbg=28 gui=none guifg=bg
hi  DiffText	cterm=bold ctermfg=green ctermbg=22 gui=none guifg=bg

