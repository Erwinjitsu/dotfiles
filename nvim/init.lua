-- Wraps remaping
function nnoremap(keys, cmd)
  vim.keymap.set('n', keys, cmd, { noremap = true, silent = true })
end

-- Remaps
nnoremap("o", "o<Esc>")
nnoremap("O", "O<Esc>")
nnoremap("x", "\"_x")
nnoremap("s", "\"_s")
nnoremap("q:", ":q")
nnoremap("<C-h>", "<C-w>h")
nnoremap("<C-j>", "<C-w>j")
nnoremap("<C-k>", "<C-w>k")
nnoremap("<C-l>", "<C-w>l")
nnoremap("<C-l>", "<C-w>l")
nnoremap("<leader>r", vim.lsp.buf.rename)
--nnoremap("^", ":TagbarToggle<CR>")

-- Other settings
vim.opt.nu = true
vim.opt.mouse = ""
vim.cmd [[colorscheme rainforest]]

-- Plugins
vim.cmd [[packadd packer.nvim]]
require('packer').startup(function()
  use 'wbthomason/packer.nvim'

  -- Some common ones
  use 'ap/vim-css-color'
  use 'preservim/tagbar'
  use 'tpope/vim-fugitive'

  -- CMP
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'saadparwaiz1/cmp_luasnip'

  -- Snippets
  use 'L3MON4D3/LuaSnip'
  use 'rafamadriz/friendly-snippets'

  -- LSP
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'
  use 'neovim/nvim-lspconfig'
end)

require('snippets')
require('lsp')
