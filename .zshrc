#  -------------------------------------------------------------------------  #
#   _____                  _             _  _  _                              #
#  | ____| _ __ __      __(_) _ __      | |(_)| |_  ___  _   _                #
#  |  _|  | '__|\ \ /\ / /| || '_ \  _  | || || __|/ __|| | | |               #
#  | |___ | |    \ V  V / | || | | || |_| || || |_ \__ \| |_| |               #
#  |_____||_|     \_/\_/  |_||_| |_| \___/ |_| \__||___/ \__,_|               #
#                                                                             #
#  FILE: zshrc                                                                #
#  Creator: Erwin                                                             #
#  -------------------------------------------------------------------------  #

zstyle ':completion:*' completer _complete _ignored _correct
zstyle ':completion:*:command' rehash 1
zstyle :compinstall filename '/home/erwin/.zshrc'

autoload -Uz compinit
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"
# End of lines added by compinstall
# Some settings
HISTFILE=~/.cache/zsh/history
HISTSIZE=1000
SAVEHIST=1000
HISTCONTROL=ignoreboth

# Vi for cli
autoload edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line
 
# For development (LBRY-GTK as example)
# TODO: Repurposed when I make some real dev tools
if (( ${+LG_BUILDER} )); then
	alias lgbuild='cd CSource && ./Setup.py reinstall && cd ..'
	alias lgrun='../../bin/lbry-gtk'
	alias lgnow='ls -Art CSource/*.c | tail -n 1 | xargs nvim'
fi

mv_r() {
	rsync --info=stats1,progress2 --remove-source-files "$@"
}
screen_key() {
	screenkey -t 0.5 -s small --font-color "#7da861" --bg-color "#282c35" --scr 1
}
# Alias Start

source ~/.config/aliasrc

alias play='ffplay -fs -autoexit'
alias pacup='doas pacman -Syu'
alias pacrm='doas pacman -Rsn'
alias pacins='doas pacman -S'
alias delswp='rm ~/.local/share/nvim/swap/*'
alias ls='ls --color=auto'
alias vim='nvim'
alias dovi='doas nvim'
alias cp="cp -i" 
alias ssh='TERM=xterm ssh'
# Alias End

## Init
setopt PROMPT_SUBST

# We want to show git info in a repo
autoload -Uz vcs_info
zstyle ':vcs_info:*' stagedstr 'M' 
zstyle ':vcs_info:*' unstagedstr 'M' 
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' actionformats '%F{70}[%F{195}%b%F{3}|%F{1}%a%F{70}]%f'
zstyle ':vcs_info:*' formats \
  '%F{70}[%F{195}%b%F{70}] %F{2}%c%F{3}%u%f'
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
zstyle ':vcs_info:*' enable git 
+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
    [[ $(git ls-files --other --directory --exclude-standard | sed q | wc -l | tr -d ' ') == 1 ]] ; then

    hook_com[unstaged]+='%f'
  fi
}

precmd () { vcs_info }

## Colorful prompt and an symbol to indicate insert/normal mode
## Options
VI_INS_MODE_SYMBOL=${VI_INS_MODE_SYMBOL:-'%F{39}%T '}
VI_CMD_MODE_SYMBOL=${VI_CMD_MODE_SYMBOL:-'%F{190}%T '}

## Set symbol for the initial mode
VI_MODE_SYMBOL="${VI_INS_MODE_SYMBOL}"

# on keymap change, define the mode and redraw prompt
zle-keymap-select() {
  if [ "${KEYMAP}" = 'vicmd' ]; then
    VI_MODE_SYMBOL="${VI_CMD_MODE_SYMBOL}"
  else
    VI_MODE_SYMBOL="${VI_INS_MODE_SYMBOL}"
  fi
  zle reset-prompt
}
zle -N zle-keymap-select

# reset to default mode at the end of line input reading
zle-line-finish() {
  VI_MODE_SYMBOL="${VI_INS_MODE_SYMBOL}"
}
zle -N zle-line-finish

# Fix a bug when you C-c in CMD mode, you'd be prompted with CMD mode indicator
# while in fact you would be in INS mode.
# Fixed by catching SIGINT (C-c), set mode to INS and repropagate the SIGINT,
# so if anything else depends on it, we will not break it.
TRAPINT() {
  VI_MODE_SYMBOL="${VI_INS_MODE_SYMBOL}"
  return $(( 170 + $1 ))
}

PROMPT='%F{170}%n $VI_MODE_SYMBOL%F{15}%1~|${vcs_info_msg_0_}%F{70}%f%B%f%b%(?.%F{70}>.%F{9}%B>%b)%F{195} '

# ls colours I chose
LS_COLORS='rs=0:di=00;32:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:';
export LS_COLORS
